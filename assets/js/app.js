/****************************/
/*Stocks*/

$(document).on('click', '#stockRemoveBtn', function() {
	alert("remove")
})



/*********************************/
/* Components and Products */
$('.comp_elem').click(function(){
	activate_btn('component')
	$('.comp_elem').removeClass('success')
	$(this).addClass('success')
})
$('#btn_comp_change').click(function(){
	var id = $('tr.comp_elem.success').attr('id')
	// alert(id)
	window.location.assign("/component/"+id+"/edit/");
})
$('#btn_comp_remove').click(function(){
	alert("Remove component")
})
$('.prod_elem').click(function(){
	activate_btn('product')
	$('.prod_elem').removeClass('success')
	$(this).addClass('success')
})
$('#btn_prod_change').click(function(){
	var id = $('tr.prod_elem.success').attr('id')
	// alert(id)
	window.location.assign("/product/"+id+"/edit/");
})
$('#btn_prod_remove').click(function(){
	alert("Remove product")
})
function activate_btn(data) {
	if (data=='component') {
		$('.comp_btn').removeClass('disabled')
	}
	if (data=='product') {
		$('.prod_btn').removeClass('disabled')
	}
}



/**************************/
/*Cashbox*/

//variables

var goods = []

//events

$(document).on('click', '.cash_prod_elem', function() {
	$('.cash_prod_elem').removeClass('success')
	$(this).addClass('success')
})
$(document).on('click', '.cash_prod_elem_check', function() {
	$('.cash_prod_elem_check').removeClass('success')
	$(this).addClass('success')
})
$(document).on('click', '.cash_prod_elem', function() {
	$('.cash_prod_elem').removeClass('success')
	$(this).addClass('success')
	// var elem = $('tr.cash_prod_elem.success');
	addGood($(this));
})
$(document).on('click', '#incBtn', function() {
	$(this).parent().click();
	var elem = $(this).parent().parent();
	changeGood(elem, 1);
})
$(document).on('click', '#decBtn', function() {
	$(this).parent().click();
	//забыть это как страшный сон
	var elem = $(this).parent().parent();
	changeGood(elem, -1)
})
$(document).on('click', '#removeBtn', function() {
	$(this).parent().click();
	var elem = $(this).parent().parent();
	removeGood(elem)
})


/*Order process*/

$('#order').on('submit', function(event){
	event.preventDefault();
	send_order();
});

function send_order() {
	var ordered_goods = []
	var ordered_counts = []
	$.each(goods, function(i, item) {
		ordered_goods.push(item[0])
		ordered_counts.push(item[1])
	})
	csrf = $('input[name="csrfmiddlewaretoken"]').val();
	json = {
		'goods': ordered_goods,
		'count': ordered_counts,
		csrfmiddlewaretoken: csrf
	}
	$.ajax({
		url: 'order/',
		type: "POST",
		data: json,
		success : function(response) {
			window.location.assign(response);
		}
	})
}
function addGood(elem) {
	var values = []

	//values - хранит пункт заказа ID, количество
	values.push(elem.attr('id'))
	values.push(1)
	
	//добавляем в values (неудачное название) Название, Цена (для вывода в списке к заказу)
	$.each(elem.find('td'), function(i, item) {
		if($.inArray(i, [0,2]) !== -1) { values.push(item.innerHTML) }
	})

	// еще одно неудачное название. промежуточный массив, хранящий id из списка к заказу
	var in_order = []
	$.each(goods, function(i, item) {
		in_order.push(item[0])
	})
	//если в нём есть товар, который мы добавляем, то count ++
	if($.inArray(values[0],in_order) !== -1) {
		var count = goods[in_order.indexOf(values[0])][1]+1
		//удобно - находим необходимый товар в массиве goods, используя массив in_order
		goods[in_order.indexOf(values[0])][1] = count
		var price = values[3]*count

		$('.cash_prod_elem_check#'+values[0]+' td').eq(1).html(count)
		$('.cash_prod_elem_check#'+values[0]+' td').eq(2).html(price)

	}
	else {
		// иначе добавляем в список товаров к заказу и в массив goods
		$('#table_checkout').append('<tr id="'+values[0]+'" class="cash_prod_elem_check"><td><button type="button" id="decBtn" class="close"><span class="glyphicon glyphicon-minus"></span></button>'+values[2]+
			'<button type="button" id="incBtn" class="close"><span class="glyphicon glyphicon-plus"></span></button></td><td>'+values[1]+
			'</td><td>'+values[3]+
			'</td><td><button type="button" id="removeBtn" class="close"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button></td></tr>')
		goods.push(values)
	}
	//console.log(goods);
}
function changeGood(elem, value) {
	var id = elem.attr('id')
	console.log(elem)
	// еще одно неудачное название. промежуточный массив, хранящий id из списка к заказу
	var in_order = []
	$.each(goods, function(i, item) {
		in_order.push(item[0])
	})
	//в нём есть товар, который мы добавляем, то count +-

	//исходные данные
	var count = goods[in_order.indexOf(id)][1]
	var price = goods[in_order.indexOf(id)][3]

	count = count + value
	if (count == 0) count = 1;
	goods[in_order.indexOf(id)][1] = count
	price = price*count

	$('.cash_prod_elem_check#'+id+' td').eq(1).html(count)
	$('.cash_prod_elem_check#'+id+' td').eq(2).html(price)

}
function removeGood(elem) {
	var id = elem.attr('id')
	var in_order = []
	$.each(goods, function(i, item) {
		in_order.push(item[0])
	})
	//удаляем выбранный товар из заказа
	goods.splice(in_order.indexOf(id), 1);
	
	//и из вёрстки тоже
	$('.cash_prod_elem_check').remove('#'+id);
}