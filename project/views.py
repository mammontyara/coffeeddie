# -*- coding: utf-8 -*-

from django.shortcuts import render, render_to_response, get_object_or_404, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.template import Context, RequestContext
from django.contrib.auth.decorators import login_required

from accounts.models import User
from project.models import Component, Product, ProductOrdered, Order, Stock, Report, DayShift
from project.forms import ComponentForm , ProductForm, StockForm, DayShiftForm

from datetime import datetime

# Create your views here.


@login_required
def list(request):
	component_list = Component.objects.order_by('component_title');
	product_list = Product.objects.order_by('product_title');
	context = ({
		'page': 'listPage',
		'component_list': component_list,
		'product_list': product_list,
		})
	return render(request, 'project/list.html', context)

@login_required
def component_edit(request, pk):
	component = get_object_or_404(Component, pk=pk)
	if request.method == "POST":
		form = ComponentForm(request.POST, instance=component)
		if form.is_valid():
			component = form.save(commit=False)
			component.save()
			return redirect('list')
	else:
		form = ComponentForm(instance=component)
	return render_to_response('project/component.html', {'form': form},  RequestContext(request))

@login_required
def component_add(request):
	if request.method == 'POST':
		form = ComponentForm(request.POST)
		if form.is_valid():
			component = form.save(commit=False)
			component.save()
	form = ComponentForm()
	context = {'form': form}
	return render_to_response('project/component.html', {'form': form},  RequestContext(request))
	# return render(request, 'project/component.html', context)

@login_required
def product_edit(request, pk):
	product = get_object_or_404(Product, pk=pk)
	if request.method == "POST":
		form = ProductForm(request.POST, instance=product)
		if form.is_valid():
			product = form.save(commit=False)
			product.save()
			return redirect('list')
	else:
		form = ProductForm(instance=product)
	return render_to_response('project/component.html', {'form': form},  RequestContext(request))

@login_required
def product_add(request):
	if request.method == 'POST':
		form = ProductForm(request.POST)
		if form.is_valid():
			product = form.save(commit=False)
			product.save()
	form = ProductForm()
	context = {'form': form}
	return render_to_response('project/component.html', {'form': form},  RequestContext(request))
	# return render(request, 'project/component.html', context)

@login_required
def cashbox(request):
	# if request.method == 'POST':
	# 	print('here')
	# 	request.session['_old_post'] = request.POST.getlist('data')
	# 	return redirect('order')
	product_list = Product.objects.order_by('product_title');
	context = ({
		'page': 'cashPage',
		'product_list': product_list,
		})
	return render(request, 'project/index.html', context)

@login_required
def make_order_ajax(request):
	#if request.POST:
	if request.is_ajax():
		good_list = request.POST.getlist('goods[]')
		count_list = request.POST.getlist('count[]')

		# список действующих акций
		today = datetime.now().date()
		stocks = []
		for stock in Stock.objects.all():
			if stock.stock_date_start <= today:
				if stock.stock_date_finish >= today:
					stocks.append(stock)

		# выполнение заказа
		if good_list:
			order = Order(order_worker=request.session.get('_auth_user_id'))
			order.save()
			for good in good_list:
				product = Product.objects.get(id=good)
				count = count_list[good_list.index(good)] # количество товаров
				price = product.product_price*int(count)
				product_ordered = ProductOrdered(product=product,count=count, price=price)
				product_ordered.save()
				order.products.add(product_ordered)
			order.save()
			
			#тут будут вмешиваться акции

			# for productOrdered in order.products.all():
			# 	for stock in stocks:
			# 		for product in stock.products:
			# 			print(product)
			# print(my_stocks)

			# for productOrdered in order.products.all():
			# 	print(productOrdered.product.productOrdered_title)
			# 	for stock in stocks:
			# 		if productOrdered.product in stock.products:
			# 			print ("YES", productOrdered.product, stock.stock_title)
			
			# assert False
			# for productOrdered in order.products.all():



			# перерасчёт стоимости
			final_price = 0
			for productOrdered in order.products.all():
				final_price = final_price + productOrdered.price
			order.order_price = final_price
			order.save()

			# мистер пропер

			# в случае успешного создания заказа - переадресация
			return HttpResponse('/order/'+str(order.id)+'/')
		return HttpResponse('/')
	return HttpResponseRedirect("/")

@login_required
def order_details(request, pk):
	order = get_object_or_404(Order, pk=pk)
	worker = get_object_or_404(User, pk=order.order_worker)
	print("SSS")
	product_list = []
	for productOrdered in order.products.all():
		product = productOrdered.product
		product_list_item = ({
			'title': product.product_title,
			'count': productOrdered.count,
			'price': product.product_price,
			'final_price': product.product_price * productOrdered.count
			})
		product_list.append(product_list_item)

	context = ({'product_list': product_list,
		'price': order.order_price,
		'worker': worker
	})
	return render(request, 'project/order.html', context)
	#return render_to_response('project/order.html', {'data':context},  RequestContext(request))

@login_required
def stock_list(request):
	stock_list = Stock.objects.order_by('stock_title');
	context = ({
		'stock_list': stock_list,
		'page': 'stockPage',
		})
	return render(request, 'project/stock_list.html', context)

@login_required
def stock_add(request):
	if request.method == 'POST':
		form = StockForm(request.POST)
		if form.is_valid():
			stock = form.save(commit=False)
			stock.save()
	form = StockForm()
	context = {'form': form}
	return render_to_response('project/component.html', {'form': form},  RequestContext(request))


@login_required
def stock_edit(request, pk):
	stock = get_object_or_404(Stock, pk=pk)
	if request.method == "POST":
		form = StockForm(request.POST, instance=stock)
		if form.is_valid():
			stock = form.save(commit=False)
			stock.save()
			return redirect('stock_list')
	else:
		form = StockForm(instance=stock)
	return render_to_response('project/component.html', {'form': form},  RequestContext(request))

@login_required
def employee_list(request):
	today = datetime.now().date()
	dayshift, created = DayShift.objects.get_or_create(dayshift_day=today)
	
	if request.method == "POST":
		form = DayShiftForm(request.POST, instance=dayshift)
		if form.is_valid():
			dayshift = form.save(commit=False)
			dayshift.save()
			return redirect('employee_list')
	form = DayShiftForm(instance=dayshift)	
	context = ({
		'page': 'employeePage',
		'dayShift': dayshift,
		'form': form,
		})
	# return render(request, 'project/employee_list.html', context)
	return render_to_response('project/employee_list.html', context,  RequestContext(request))

@login_required
def employee_list_specific(request, year, month, day):
	today = datetime(int(year), int(month), int(day))
	dayshift, created = DayShift.objects.get_or_create(dayshift_day=today)
	if request.method == "POST":
		form = DayShiftForm(request.POST, instance=dayshift)
		if form.is_valid():
			dayshift = form.save(commit=False)
			dayshift.save()
			return redirect('employee_list_specific', year, month, day)
	form = DayShiftForm(instance=dayshift)	
	context = ({
		'page': 'employeePage',
		'dayShift': dayshift,
		'form': form,
		})
	return render_to_response('project/employee_list.html', context,  RequestContext(request))

	# product = get_object_or_404(Product, pk=pk)
	# if request.method == "POST":
	# 	form = ProductForm(request.POST, instance=product)
	# 	if form.is_valid():
	# 		product = form.save(commit=False)
	# 		product.save()
	# 		return redirect('list')
	# else:
	# 	form = ProductForm(instance=product)
	# return render_to_response('project/component.html', {'form': form},  RequestContext(request))

@login_required
def reports(request):
	report_list = Report.objects.order_by('date_add');
	context = ({
		'page': 'reportPage',
		'report_list': report_list,
		})
	return render(request, 'project/report.html', context)


# some information

def about(request):
	return render(request, 'project/about.html')