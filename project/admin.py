from django.contrib import admin
from project.models import Component, Product, ProductOrdered, Order, Stock, Report, DayShift

# Register your models here.

class ComponentAdmin(admin.ModelAdmin):
	list_display = ('id','component_title', 'component_type', 'component_count','component_price',)
	search_fields = ('component_title', 'component_type',)

class ProductAdmin(admin.ModelAdmin):
	list_display = ('product_title', 'product_type', 'product_price',)
	search_fields = ('product_title', 'product_type',)
	filter_horizontal = ['components']

class ProductOrderedAdmin(admin.ModelAdmin):
	list_display = ('get_product',)

class OrderAdmin(admin.ModelAdmin):
	list_display = ('get_products',)
	filter_horizontal = ['products']

class StockAdmin(admin.ModelAdmin):
	filter_horizontal = ['products']



admin.site.register(Component,ComponentAdmin)
admin.site.register(Product,ProductAdmin)
admin.site.register(ProductOrdered, ProductOrderedAdmin)
admin.site.register(Order, OrderAdmin)
admin.site.register(Stock, StockAdmin)
admin.site.register(Report)
admin.site.register(DayShift)