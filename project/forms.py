# -*- coding: utf-8 -*-

from django.forms import ModelForm
from django.db import models
from project.models import Component, Product, Stock, DayShift

class ComponentForm(ModelForm):
	class Meta:
		model = Component
		fields = ['component_title', 'component_count', 'component_price', 'component_type']

class ProductForm(ModelForm):
	class Meta:
		model = Product
		fields = ['product_title', 'product_price', 'product_type', 'components']

class StockForm(ModelForm):
	class Meta:
		model = Stock
		fields = ['stock_title', 'stock_discount', 'stock_date_start', 'stock_date_finish', 'products']


class DayShiftForm(ModelForm):
	class Meta:
		model = DayShift
		fields = ['dayshift_one','dayshift_two','dayshift_three','dayshift_four','dayshift_five','dayshift_six']		



# class OrderForm(ModelForm):
# 	class Meta:
# 		model = Order
# 		fields = ['products','order_price', 'order_worker']

