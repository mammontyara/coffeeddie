# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0010_auto_20141122_1226'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='order_worker',
            field=models.IntegerField(max_length=9),
            preserve_default=True,
        ),
    ]
