# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0015_auto_20141202_2224'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='product',
            options={'verbose_name': '\u041f\u0440\u043e\u0434\u0443\u043a\u0442', 'verbose_name_plural': '\u041f\u0440\u043e\u0434\u0443\u043a\u0442\u044b'},
        ),
        migrations.RemoveField(
            model_name='stock',
            name='stock_is_eternal',
        ),
    ]
