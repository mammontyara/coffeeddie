# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0008_stock'),
    ]

    operations = [
        migrations.AddField(
            model_name='stock',
            name='date_finish',
            field=models.DateTimeField(null=True, verbose_name='\u0414\u0430\u0442\u0430 \u0437\u0430\u0432\u0435\u0440\u0448\u0435\u043d\u0438\u044f', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='stock',
            name='date_start',
            field=models.DateTimeField(null=True, verbose_name='\u0414\u0430\u0442\u0430 \u043d\u0430\u0447\u0430\u043b\u0430', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='order',
            name='order_worker',
            field=models.IntegerField(max_length=8),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='productordered',
            name='product',
            field=models.ForeignKey(to='project.Product'),
            preserve_default=True,
        ),
    ]
