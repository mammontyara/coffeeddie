# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0011_auto_20141122_1227'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='stock',
            options={},
        ),
        migrations.RemoveField(
            model_name='stock',
            name='date_finish',
        ),
        migrations.RemoveField(
            model_name='stock',
            name='date_start',
        ),
        migrations.RemoveField(
            model_name='stock',
            name='products',
        ),
        migrations.RemoveField(
            model_name='stock',
            name='stock_discount',
        ),
        migrations.RemoveField(
            model_name='stock',
            name='stock_title',
        ),
        migrations.AlterField(
            model_name='order',
            name='order_worker',
            field=models.IntegerField(max_length=8),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='productordered',
            name='product',
            field=models.IntegerField(max_length=8),
            preserve_default=True,
        ),
    ]
