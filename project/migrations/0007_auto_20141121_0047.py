# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0006_auto_20141119_2016'),
    ]

    operations = [
        migrations.AlterField(
            model_name='productordered',
            name='product',
            field=models.IntegerField(max_length=8),
            preserve_default=True,
        ),
    ]
