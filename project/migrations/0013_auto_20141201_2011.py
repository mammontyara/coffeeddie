# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0012_auto_20141122_1506'),
    ]

    operations = [
        migrations.CreateModel(
            name='Report',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterModelOptions(
            name='stock',
            options={'verbose_name': '\u0410\u043a\u0446\u0438\u044f', 'verbose_name_plural': '\u0410\u043a\u0446\u0438\u0438'},
        ),
        migrations.AddField(
            model_name='stock',
            name='date_add',
            field=models.DateField(default=datetime.date.today, verbose_name='\u0414\u0430\u0442\u0430 \u0434\u043e\u0431\u0430\u0432\u043b\u0435\u043d\u0438\u044f', auto_now=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='stock',
            name='products',
            field=models.ManyToManyField(to='project.Product'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='stock',
            name='stock_date_finish',
            field=models.DateField(default=datetime.date.today, verbose_name='\u0414\u0430\u0442\u0430 \u043e\u043a\u043e\u043d\u0447\u0430\u043d\u0438\u044f \u0434\u0435\u0439\u0441\u0442\u0432\u0438\u044f \u0430\u043a\u0446\u0438\u0438'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='stock',
            name='stock_date_start',
            field=models.DateField(default=datetime.date.today, verbose_name='\u0414\u0430\u0442\u0430 \u043d\u0430\u0447\u0430\u043b\u0430 \u0434\u0435\u0439\u0441\u0442\u0432\u0438\u044f \u0430\u043a\u0446\u0438\u0438'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='stock',
            name='stock_discount',
            field=models.IntegerField(default=0, max_length=3),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='stock',
            name='stock_is_eternal',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='stock',
            name='stock_title',
            field=models.CharField(default=b'', max_length=50, verbose_name='\u041d\u0430\u0438\u043c\u0435\u043d\u043e\u0432\u0430\u043d\u0438\u0435'),
            preserve_default=True,
        ),
    ]
