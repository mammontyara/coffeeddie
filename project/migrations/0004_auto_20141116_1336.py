# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0003_auto_20141115_2343'),
    ]

    operations = [
        migrations.AddField(
            model_name='component',
            name='component_type',
            field=models.CharField(default=b'\xd0\xa0\xd0\x90\xd0\xa1\xd0\xa5\xd0\x9e\xd0\x94\xd0\x9d\xd0\x98\xd0\x9a', max_length=20, choices=[('\u0412\u042b\u041f\u0415\u0427\u041a\u0410', '\u0412\u044b\u043f\u0435\u0447\u043a\u0430'), ('\u041d\u0410\u041f\u0418\u0422\u041a\u0418', '\u041d\u0430\u043f\u0438\u0442\u043a\u0438'), ('\u0421\u041f\u0415\u0426\u0418\u0418', '\u041f\u0440\u0438\u043f\u0440\u0430\u0432\u044b \u0438 \u0441\u043f\u0435\u0446\u0438\u0438'), ('\u0420\u0410\u0421\u0425\u041e\u0414\u041d\u0418\u041a', '\u0420\u0430\u0441\u0445\u043e\u0434\u043d\u044b\u0435 \u043c\u0430\u0442\u0435\u0440\u0438\u0430\u043b\u044b')]),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='product',
            name='product_type',
            field=models.CharField(default=b'\xd0\xa0\xd0\x90\xd0\xa1\xd0\xa5\xd0\x9e\xd0\x94\xd0\x9d\xd0\x98\xd0\x9a', max_length=20, choices=[('\u0412\u042b\u041f\u0415\u0427\u041a\u0410', '\u0412\u044b\u043f\u0435\u0447\u043a\u0430'), ('\u041d\u0410\u041f\u0418\u0422\u041a\u0418', '\u041d\u0430\u043f\u0438\u0442\u043a\u0438'), ('\u0421\u041f\u0415\u0426\u0418\u0418', '\u041f\u0440\u0438\u043f\u0440\u0430\u0432\u044b \u0438 \u0441\u043f\u0435\u0446\u0438\u0438'), ('\u0420\u0410\u0421\u0425\u041e\u0414\u041d\u0418\u041a', '\u0420\u0430\u0441\u0445\u043e\u0434\u043d\u044b\u0435 \u043c\u0430\u0442\u0435\u0440\u0438\u0430\u043b\u044b')]),
            preserve_default=True,
        ),
    ]
