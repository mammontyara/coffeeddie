# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0013_auto_20141201_2011'),
    ]

    operations = [
        migrations.AlterField(
            model_name='productordered',
            name='product',
            field=models.ForeignKey(to='project.Product'),
            preserve_default=True,
        ),
    ]
