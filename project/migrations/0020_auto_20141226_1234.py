# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0019_auto_20141226_1142'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='dayshift',
            options={'verbose_name': '\u0420\u0430\u0431\u043e\u0447\u0430\u044f \u0441\u043c\u0435\u043d\u0430', 'verbose_name_plural': '\u0420\u0430\u0431\u043e\u0447\u0438\u0435 \u0441\u043c\u0435\u043d\u044b'},
        ),
    ]
