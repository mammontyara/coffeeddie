# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0014_auto_20141201_2023'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='report',
            options={'verbose_name': '\u041e\u0442\u0447\u0451\u0442', 'verbose_name_plural': '\u041e\u0442\u0447\u0451\u0442\u044b'},
        ),
        migrations.AddField(
            model_name='report',
            name='date_add',
            field=models.DateField(default=datetime.datetime(2014, 12, 2, 19, 24, 23, 77000, tzinfo=utc), verbose_name='\u0414\u0430\u0442\u0430 \u0432\u044b\u043f\u043e\u043b\u043d\u0435\u043d\u0438\u044f', auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='report',
            name='report_category',
            field=models.CharField(default=b'\xd0\x97\xd0\x90\xd0\x9a\xd0\x90\xd0\x97\xd0\xab', max_length=20, choices=[('\u0417\u0410\u041a\u0410\u0417\u042b', '\u0417\u0430\u043a\u0430\u0437\u044b'), ('\u0417\u0410\u041a\u0423\u041f\u041a\u0418', '\u0417\u0430\u043a\u0443\u043f\u043a\u0438'), ('\u041f\u041e\u041b\u042c\u0417\u041e\u0412\u0410\u0422\u0415\u041b\u0418', '\u041f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u0438'), ('\u0410\u041a\u0426\u0418\u0418', '\u0410\u043a\u0446\u0438\u0438')]),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='report',
            name='report_price',
            field=models.DecimalField(default=0, max_digits=8, decimal_places=2),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='report',
            name='report_title',
            field=models.CharField(default='some', max_length=50, verbose_name='\u041d\u0430\u0438\u043c\u0435\u043d\u043e\u0432\u0430\u043d\u0438\u0435'),
            preserve_default=False,
        ),
    ]
