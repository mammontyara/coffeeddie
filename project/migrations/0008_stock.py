# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0007_auto_20141121_0047'),
    ]

    operations = [
        migrations.CreateModel(
            name='Stock',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('stock_title', models.CharField(max_length=50, verbose_name='\u041d\u0430\u0438\u043c\u0435\u043d\u043e\u0432\u0430\u043d\u0438\u0435')),
                ('stock_discount', models.IntegerField(max_length=8)),
                ('products', models.ManyToManyField(to='project.Product')),
            ],
            options={
                'verbose_name': '\u0410\u043a\u0446\u0438\u044f',
                'verbose_name_plural': '\u0410\u043a\u0446\u0438\u0438',
            },
            bases=(models.Model,),
        ),
    ]
