# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0005_remove_product_product_count'),
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('order_price', models.DecimalField(default=0, max_digits=8, decimal_places=2)),
                ('order_worker', models.IntegerField(max_length=8)),
                ('date_add', models.DateField(auto_now=True, verbose_name='\u0414\u0430\u0442\u0430 \u0432\u044b\u043f\u043e\u043b\u043d\u0435\u043d\u0438\u044f')),
            ],
            options={
                'verbose_name': '\u0417\u0430\u043a\u0430\u0437',
                'verbose_name_plural': '\u0417\u0430\u043a\u0430\u0437\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ProductOrdered',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('count', models.IntegerField(max_length=8)),
                ('price', models.DecimalField(default=0, max_digits=8, decimal_places=2)),
                ('product', models.ForeignKey(to='project.Product')),
            ],
            options={
                'verbose_name': '\u0417\u0430\u043a\u0430\u0437\u0430\u043d\u043d\u044b\u0439 \u043f\u0440\u043e\u0434\u0443\u043a\u0442',
                'verbose_name_plural': '\u0417\u0430\u043a\u0430\u0437\u0430\u043d\u043d\u044b\u0435 \u043f\u0440\u043e\u0434\u0443\u043a\u0442\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='order',
            name='products',
            field=models.ManyToManyField(to='project.ProductOrdered'),
            preserve_default=True,
        ),
    ]
