# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0016_auto_20141219_1321'),
    ]

    operations = [
        migrations.AddField(
            model_name='report',
            name='report_content',
            field=models.IntegerField(default=0, max_length=8),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='report',
            name='report_category',
            field=models.CharField(default=b'\xd0\x97\xd0\x90\xd0\x9a\xd0\x90\xd0\x97', max_length=20, choices=[('\u0417\u0410\u041a\u0410\u0417', '\u0417\u0430\u043a\u0430\u0437'), ('\u0417\u0410\u041a\u0423\u041f\u041a\u0410', '\u0417\u0430\u043a\u0443\u043f\u043a\u0430'), ('\u041f\u041e\u041b\u042c\u0417\u041e\u0412\u0410\u0422\u0415\u041b\u042c', '\u041f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044c'), ('\u041a\u041e\u041c\u041f\u041e\u041d\u0415\u041d\u0422', '\u041a\u043e\u043c\u043f\u043e\u043d\u0435\u043d\u0442'), ('\u041f\u0420\u041e\u0414\u0423\u041a\u0422', '\u041f\u0440\u043e\u0434\u0443\u043a\u0442'), ('\u0410\u041a\u0426\u0418\u042f', '\u0410\u043a\u0446\u0438\u044f')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='report',
            name='report_title',
            field=models.CharField(max_length=50, verbose_name='\u0414\u0435\u0442\u0430\u043b\u0438'),
            preserve_default=True,
        ),
    ]
