# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0004_auto_20141116_1336'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='product',
            name='product_count',
        ),
    ]
