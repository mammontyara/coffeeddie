# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0002_auto_20141115_1800'),
    ]

    operations = [
        migrations.RenameField(
            model_name='product',
            old_name='component_count',
            new_name='product_count',
        ),
        migrations.AddField(
            model_name='component',
            name='component_price',
            field=models.DecimalField(default=0, max_digits=8, decimal_places=2),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='product',
            name='product_price',
            field=models.DecimalField(default=0, max_digits=8, decimal_places=2),
            preserve_default=True,
        ),
    ]
