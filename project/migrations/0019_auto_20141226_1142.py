# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('project', '0018_report_report_user'),
    ]

    operations = [
        migrations.CreateModel(
            name='DayShift',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('dayshift_day', models.DateField(unique=True, max_length=20, verbose_name='\u0414\u0435\u043d\u044c')),
                ('dayshift_five', models.ForeignKey(related_name='dayshift_five', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('dayshift_four', models.ForeignKey(related_name='dayshift_four', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('dayshift_one', models.ForeignKey(related_name='dayshift_one', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('dayshift_six', models.ForeignKey(related_name='dayshift_six', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('dayshift_three', models.ForeignKey(related_name='dayshift_three', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('dayshift_two', models.ForeignKey(related_name='dayshift_two', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='report',
            name='report_category',
            field=models.CharField(default=b'\xd0\x97\xd0\xb0\xd0\xba\xd0\xb0\xd0\xb7', max_length=20, choices=[('\u0417\u0430\u043a\u0430\u0437', '\u0417\u0430\u043a\u0430\u0437'), ('\u0417\u0430\u043a\u0443\u043f\u043a\u0430', '\u0417\u0430\u043a\u0443\u043f\u043a\u0430'), ('\u041f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044c', '\u041f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044c'), ('\u041a\u043e\u043c\u043f\u043e\u043d\u0435\u043d\u0442', '\u041a\u043e\u043c\u043f\u043e\u043d\u0435\u043d\u0442'), ('\u041f\u0440\u043e\u0434\u0443\u043a\u0442', '\u041f\u0440\u043e\u0434\u0443\u043a\u0442'), ('\u0410\u043a\u0446\u0438\u044f', '\u0410\u043a\u0446\u0438\u044f')]),
            preserve_default=True,
        ),
    ]
