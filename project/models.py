#!/usr/bin/python
# -*- coding: utf-8 -*-

from django.db import models
from accounts.models import User
import datetime

# Create your models here.

class Component(models.Model):
	COMP_CHOICES = (
		(u'ВЫПЕЧКА',u'Выпечка'),
		(u'НАПИТКИ',u'Напитки'),
		(u'СПЕЦИИ',u'Приправы и специи'),
		(u'РАСХОДНИК',u'Расходные материалы'),
	)
	component_title = models.CharField(u'Наименование', max_length=50)
	component_count = models.IntegerField(max_length=10)
	component_price = models.DecimalField(max_digits=8, decimal_places=2, default=0)
	component_type = models.CharField(max_length=20, choices=COMP_CHOICES, default='РАСХОДНИК')
	date_add = models.DateField(u'Дата добавления', auto_now=True)

	class Meta:
		verbose_name_plural="Компоненты"
		verbose_name='Компонент'

	def __unicode__(self):
	    return u'%s' % (self.component_title)

class Product(models.Model):
	PRODUCT_CHOICES = (
		(u'ВЫПЕЧКА',u'Выпечка'),
		(u'НАПИТКИ',u'Напитки'),
		(u'СПЕЦИИ',u'Приправы и специи'),
		(u'РАСХОДНИК',u'Расходные материалы'),
	)
	product_title = models.CharField(u'Наименование', max_length=50)
	# product_count = models.IntegerField(max_length=10)
	product_price = models.DecimalField(max_digits=8, decimal_places=2, default=0)
	product_type = models.CharField(max_length=20, choices=PRODUCT_CHOICES, default='РАСХОДНИК')
	date_add = models.DateField(u'Дата добавления', auto_now=True)
	components = models.ManyToManyField(Component)

	class Meta:
		verbose_name_plural='Продукты'
		verbose_name='Продукт'

	def __unicode__(self):
	    return u'%s' % (self.product_title)

class ProductOrdered(models.Model):
	product = models.ForeignKey(Product)
	count = models.IntegerField(max_length=8)
	price = models.DecimalField(max_digits=8, decimal_places=2, default=0)

	class Meta:
		verbose_name_plural='Заказанные продукты'
		verbose_name='Заказанный продукт'

	def get_product(self):
	 	return self.product.product_title+" x"+str(self.count)

	def __unicode__(self):
	    return u'%s x%s' % (self.product.product_title,str(self.count))

class Order(models.Model):
	products = models.ManyToManyField(ProductOrdered)
	order_price = models.DecimalField(max_digits=8, decimal_places=2, default=0)
	order_worker = models.IntegerField(max_length=8)
	date_add = models.DateField(u'Дата выполнения', auto_now=True)

	class Meta:
		verbose_name_plural='Заказы'
		verbose_name='Заказ'

	def get_products(self):
		return " | ".join([p.product.product_title+" x"+str(p.count) for p in self.products.all()])

class Stock(models.Model):
	stock_title = models.CharField(u'Наименование', max_length=50, default="")
	stock_discount = models.IntegerField(max_length=3, default=0)
	# stock_is_eternal = models.BooleanField(default=False)
	stock_date_start = models.DateField(u'Дата начала действия акции', default=datetime.date.today)
	stock_date_finish = models.DateField(u'Дата окончания действия акции', default=datetime.date.today)

	products = models.ManyToManyField(Product)
	date_add = models.DateField(u'Дата добавления', auto_now=True, default=datetime.date.today)
	class Meta:
		verbose_name_plural='Акции'
		verbose_name='Акция'
		
	def __unicode__(self):
	    return u'%s' % (self.stock_title)

class Report(models.Model):
	REPORT_CHOICES = (
		(u'Заказ',u'Заказ'),
		(u'Закупка',u'Закупка'),
		(u'Пользователь',u'Пользователь'),
		(u'Компонент',u'Компонент'),
		(u'Продукт',u'Продукт'),
		(u'Акция',u'Акция'),
	)
	report_category = models.CharField(max_length=20, choices=REPORT_CHOICES, default='Заказ')
	report_title = models.CharField(u'Детали', max_length=50)
	report_price = models.DecimalField(max_digits=8, decimal_places=2, default=0)
	report_content = models.IntegerField(max_length=8, default=0)
	report_user = models.ForeignKey(User, blank=True, null=True)
	date_add = models.DateField(u'Дата выполнения', auto_now=True)

	class Meta:
		verbose_name_plural='Отчёты'
		verbose_name='Отчёт'

	def __unicode__(self):
	    return u'%s' % (self.report_title)

# мрак
class DayShift(models.Model):
	dayshift_day = models.DateField(u'День', max_length=20, unique=True)
	dayshift_one = models.ForeignKey(User, related_name='dayshift_one', blank=True, null=True)
	dayshift_two = models.ForeignKey(User, related_name='dayshift_two', blank=True, null=True)
	dayshift_three = models.ForeignKey(User, related_name='dayshift_three', blank=True, null=True)
	dayshift_four = models.ForeignKey(User, related_name='dayshift_four', blank=True, null=True)
	dayshift_five = models.ForeignKey(User, related_name='dayshift_five', blank=True, null=True)
	dayshift_six = models.ForeignKey(User, related_name='dayshift_six', blank=True, null=True)

	class Meta:
		verbose_name_plural='Рабочие смены'
		verbose_name='Рабочая смена'
		
	def __unicode__(self):
	    return u'%s' % (self.dayshift_day)