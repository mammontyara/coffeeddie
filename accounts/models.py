#!/usr/bin/python
# -*- coding: utf-8 -*-

from django.db import models
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser


# Create your models here.

class UserManager(BaseUserManager):
	def create_user(self, email, username, password=None):
		if not email:
			raise ValueError(u'Пользователь должен иметь e-mail')
		user = self.model(
			email=UserManager.normalize_email(email),
			username=username,
		)
		user.set_password(password)
		user.save(using=self._db)
		return user

	def create_superuser(self, email, username, password):
		user = self.create_user(
			email,
			password = password,
			username = username,
		)
		user.is_admin = True
		user.save(using=self._db)
		return user


class User(AbstractBaseUser):
	username = models.CharField(u'Логин', max_length=30, unique=True)
	email = models.EmailField(u'Электронная почта',max_length=255, unique=True)
	
	# for this needs to install Pillow
	# avatar = models.ImageField(verbose_name='Аватар',  upload_to='images/%Y/%m/%d', blank=True, null=True)
	
	first_name = models.CharField(u'Имя', max_length=30, blank=True)
	last_name = models.CharField(u'Фамилия', max_length=50,blank=True)
	date_birth = models.DateField(u'День рождения', blank=True, null=True)
	date_reg = models.DateTimeField(u'Дата регистрации', auto_now=True)
	is_active = models.BooleanField(default=True)
	is_admin = models.BooleanField(default=False)

	class Meta:
		verbose_name_plural="Сотрудники"
		verbose_name="Сотрудник"

	USERNAME_FIELD = 'email'
	REQUIRED_FIELDS = ['username']

	objects = UserManager()

	def get_full_name(self):
		return '%s %s' % (self.first_name, self.last_name,)
	def get_short_name(self):
		return self.username
	def __unicode__(self):
		return '%s %s' % (self.first_name, self.last_name,)

	def has_perm(self, perm, obj=None):
		return True
	def has_module_perms(self, app_label):
		return True

	@property
	def is_staff(self):
		return self.is_admin