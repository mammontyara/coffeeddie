from django.shortcuts import render, render_to_response
from accounts.models import User
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django.views.generic.base import TemplateResponse

from accounts.forms import UserLoginForm
# Create your views here.


def log_in(request):
	if('email' in request.REQUEST) and ('password' in request.REQUEST):
		email = request.REQUEST['email']
		password = request.REQUEST['password']
		user = authenticate(email=email, password=password)
		if user is not None:
			login(request, user)
			return HttpResponseRedirect('/')
		else:
			pass
			# Return a 'invalid login' error message
	return render_to_response('accounts/login.html')

# def user_login_view(request):
# 	form = UserLoginForm(request.POST or None)
# 	context = {'form': form,}
# 	if request.method == 'POST' and form.is_valid():
# 		email = form.cleaned_data.get('email', None)
# 		password = form.cleaned_data.get('password', None)
# 		user = auth.authenticate(email=email, password=password)
# 		if user and user.is_active:
# 			auth.login(request, user)
# 			return redirect('accounts')
# 	return render(request, 'accounts/login.html', context)


@login_required
def log_out(request):
	logout(request)
	return HttpResponseRedirect('/login/')
	# return HttpResponseRedirect('http://1207.0.0.1:8000/accounts/')