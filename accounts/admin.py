# -*- coding: utf-8 -*-

from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin

from accounts.models import User
from accounts.forms import UserChangeForm, UserRegistrationForm

# Register your models here.

class UserAdmin(UserAdmin):
	form = UserChangeForm
	add_form = UserRegistrationForm

	list_display = ('email', 'username', 'is_admin',)
	list_filter = ('is_admin',)
	fieldsets = (
		(None, {'fields': ('email', 'username','password',)}),
		('Personal info', {'fields': ( 'first_name', 'last_name','date_birth',)}),
		('Permissions', {'fields': ('is_admin',)}),
		('Important Details', {'fields':('last_login',)})
	)
	add_fieldsets = (
		(None, {
			'classes': ('wide',),
			'fields': ('email', 'date_birth', 'password1', 'password2')
		}), # IT MAKES ME CRAZY!! If I'll remove this point,
		#everything is going to blow up
	)
	search_fields = ('email','username',)
	ordering = ('email',)
	filter_horizontal = ()

admin.site.register(User, UserAdmin)
admin.site.unregister(Group)