# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url

from accounts import views

urlpatterns = patterns('',
	url(r'^$', views.index, name='index'),

	# register 
	# url(r'^register/$', views.register, name='register'),
	# test
	# url(r'^users/$', views.users, name='users'),
	# test ex: /2/
	# url(r'^(?P<user_id>\w+)/$', views.user, name='user')
)