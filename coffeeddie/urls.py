from django.conf.urls import patterns, include, url
from django.contrib import admin
 
urlpatterns = patterns('',

    #common
    url(r'^admin/', include(admin.site.urls)),
    url(r'^admin', include(admin.site.urls)),
    
    url(r'^login/$', ('accounts.views.log_in'), name='log_in'),
    url(r'^login$', ('accounts.views.log_in'), name='log_in'),

    url(r'^logout/$', ('accounts.views.log_out'), name='log_out'),
    url(r'^logout$', ('accounts.views.log_out'), name='log_out'),

    #main
    url(r'^$', ('project.views.cashbox'), name='cashbox'),

    url(r'^list/$', ('project.views.list'), name='list'),
    url(r'^list$', ('project.views.list'), name='list'),

    url(r'^component/add/$', 'project.views.component_add', name='component_add'),
    url(r'^component/add$', 'project.views.component_add', name='component_add'),
    url(r'^component/(?P<pk>\d+)/edit/$', ('project.views.component_edit'), name='component_edit'),
    url(r'^component/(?P<pk>\d+)/edit$', ('project.views.component_edit'), name='component_edit'),
    
    url(r'^product/add/$', 'project.views.product_add', name='product_add'),
    url(r'^product/add$', 'project.views.product_add', name='product_add'),
    url(r'^product/(?P<pk>\d+)/edit/$', ('project.views.product_edit'), name='product_edit'),
    url(r'^product/(?P<pk>\d+)/edit$', ('project.views.product_edit'), name='product_edit'),
    
    url(r'^order/$', ('project.views.make_order_ajax'), name='order'),
    url(r'^order$', ('project.views.make_order_ajax'), name='order'),
    url(r'^order/(?P<pk>\d+)/$', ('project.views.order_details'), name='order_details'),

    url(r'^employees/$', ('project.views.employee_list'), name='employee_list'),
    url(r'^employees$', ('project.views.employee_list'), name='employee_list'),

    url(r'^employees/(?P<year>\d+)/(?P<month>\d+)/(?P<day>\d+)$', ('project.views.employee_list_specific'), name='employee_list_specific'),
    url(r'^employees/(?P<year>\d+)/(?P<month>\d+)/(?P<day>\d+)/$', ('project.views.employee_list_specific'), name='employee_list_specific'),

    url(r'^stocks/$', ('project.views.stock_list'), name='stock_list'),
    url(r'^stocks$', ('project.views.stock_list'), name='stock_list'),
    url(r'^stocks/add/$', ('project.views.stock_add'), name='stock_add'),
    url(r'^stocks/add$', ('project.views.stock_add'), name='stock_add'),
    url(r'^stocks/(?P<pk>\d+)/edit/$', ('project.views.stock_edit'), name='stock_edit'),
    url(r'^stocks/(?P<pk>\d+)/edit$', ('project.views.stock_edit'), name='stock_edit'),

    url(r'^reports/$', ('project.views.reports'), name='reports'),
    url(r'^reports$', ('project.views.reports'), name='reports'),

    url(r'^about/$', ('project.views.about'), name='about'),
    url(r'^about$', ('project.views.about'), name='about'),
)
